GeoModel Tools Library
======================

This package builds the GeoModel Tools library for the offline software of ATLAS.

The library's sources are taken from https://gitlab.cern.ch/GeoModelDev/GeoModelTools
