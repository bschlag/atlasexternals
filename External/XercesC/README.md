Xerces-C
========

This package builds [Xerces-C](https://xerces.apache.org/xerces-c/) for the
"standalone" releases that need it.
