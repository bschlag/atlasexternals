lwtnn
=======

This package builds LightWeight Neural Net (lwtnn), a 
package that reconstructs NN's trained with Keras.
See https://github.com/lwtnn/lwtnn for more information.
